
### 项目介绍     
    
 **smart shop电商业务中台** 是一款经过**百万真实用户检验**的系统，基于JDK17、spring cloud、Mybatis、Nacos、Redisson、Redis、Canal的微服务框架，采用稳定核心框架主流互联网技术架构，中台系统将通过订单中心、会员中心、营销中心、商家中心、商品中心、财务中心等模式管理。支持多种业务属性的共性能力，围绕”服务复用”来构建大中台、小前台的设计理念，实现快速部署。


我们希望能够通过gitee平台让大家更多的了解我们启山智软的产品，欢迎大家积极交流沟通，获得大家认可的同时能对产品提出宝贵的意见，帮助我们不断迭代优化系统，更好地服务于用户。

交流需求，交流业务，交流技术 (如果对您有帮助，可以点击右上角 **“star”** 支持一下哦 :+1: )
| smart shop电商业务中台 | 社区团购  | 技术微信 |
| --------------- | ------------- | ------------- |
|![](.gitee/1.jpg)|![](.gitee/1cfa98049805b78d52324346ac3a3d9.png)|![](.gitee/13cf39804464f8ae903dcac6d06ef67.png)
| QQ： 458320504   | QQ： 556731103  | VX：18106661091  |


### 项目地址

商家端 :https://gitee.com/qisange/basemall/tree/master/group-mall-admin-source

小程序端 :https://gitee.com/qisange/basemall/tree/master/group-shop-uniapp-wx-Source

后端 :https://gitee.com/qisange/basemall/tree/master/gruul

数据库 ：https://gitee.com/qisange/basemall/tree/master/gruul/init-default-sql


### 技术选型

| 技术                          | 说明                     | 技术                       | 说明                   |
| -------------------------     | -------------------     | ----------------------     | --------------------   |
| Spring Cloud                  | 核心框架                 | sentinel                   | 熔断限流             |
| mybatis-plus                  | orm增强框架              | dubbo 3.1.3                | 服务调用             |
| nacos                         | 服务注册发现/配置中心     | shardingjdbc                | 分库分表            |
| spring-cloud-gateway          | 网关                     | dynamic-datasource         |  读写分离            |
| spring-cloud-loadbalance      | 负载均衡                 |  redis                      | 分布式缓存          | 
| redisson                      | 分布式锁                 |  rabbitmq                   |  消息队列           |
| xxl-job                       | 任务调度                 |  elasticsearch              |  数据搜索           |
| oss                           | 文件上传                 |  canal                      |  数据库同步         |
| skywalking                    | 日志追踪                 |  smart-doc                  |  api文档            |


### 业务架构图

| ![输入图片说明](.gitee/222.png) |
|--|


### 模块说明

- gruul-mall-parent --通用依赖模块          
- gruul-gateway --网关
- addon-coupon --优惠券模块
- addon-skill --秒杀模块
- addon-member --会员插件
- addon-distribute --分销模块
- gruul-mall-add-platform --平台模块
- gruul-mall-carrier-pigeon --信鸽（消息中心)
- gruul-mall-cart -- 购物车服务
- gruul-mall-order --订单模块
- gruul-mall-payment --支付模块
- gruul-mall-search --检索模块
- gruul-mall-afs --售后模块
- gruul-mall-shop --店铺服务模块
- gruul-mall-storage --仓储服务模块
- gruul-mall-system --系统配置模块
- gruul-mall-uaa --认证与授权服务模块
- gruul-mall-user --用户服务模块
- gruul-mall-freight --运输服务模块
- gruul-mall-goods --商品服务模块
- gruul-mall-live --直播服务模块
- gruul-mall-overview --概况服务模块


### 平台亮点

1.大中台小前端架构思想，基于java服务的热插拔

2.支持单体部署和微服务部署切换

3.B2B B2C B2B2C等商业模式切换

4.PC端、H5端、小程序端可视化DIY店铺装修，不同渠道不同风格

5.强大灵活的权限配置，可以控制到每个菜单

6.内置自主研发客服系统，可以对接企微客服、腾讯客服、小程序客服

7.支持在线升级，轻松体验到最先版本



### 授权

smart shop开源版的核心框架是一款完全免费的开源商城系统，拥有单体和开源二种授权框架，轻量级采用前后端分离、分布式微服务架构的java商城系统。

除了开源版本，付费版smart shop电商业务中台有B2B2C和B2C商城等，呈现一体化多终端：APP+小程序+H5+PC，有关产品详情可访问官网（ www.bgniao.cn ）查看。



###  演示站
| 平台端后台演示地址: https://b2b2c.bgniao.cn/platform   |
|--|
| 商家端后台演示地址: https://b2b2c.bgniao.cn/shop       |

#### 移动端演示

| H5端二维码 | 小程序二维码 |
| --------------- | ------------- |
|![](.gitee/H5.png)|![](.gitee/cffa27a9394ff9bcb9128a7dc8166aa.jpg)

#### 前端页面截图

| ![](.gitee/%E6%9C%AA%E6%A0%87%E9%A2%98-1.png) |
|--|

#### 特别鸣谢

项目发起人：启三哥

产品经理：美子

系统总架构师：宝哥 

前端开发：罗天师、芝麻、辣条

后端开发：奋斗者、宝哥、无敌、琉璃、小石

测试：十又 、聂小倩、佩琪

运维：不吃辣的子奇

